variable "environment" {
  type        = string
  description = "Name of the environment the resource(s) will be a part of."
}

variable "lifespan" {
  type        = string
  default     = "permanent"
  description = "(Optional; Default: permanent) The intended lifespan of the resource(s)."
}

variable "subnet_type" {
  type        = string
  default     = "private"
  description = "(Optional; Default: private) Specifies which subnet, either `public`, `private`, or `data`, the instance(s) are launched in."
}

variable "manager_role" {
  type        = string
  default     = "manager"
  description = "(Optional; Default: manager) Name of the role the manager will be performing."
}

variable "manager_instance_type" {
  type        = string
  description = "The type of EC2 instance to use for the manager. If the instance type changes, a new launch configuration will be created and added to the ASG."
}

variable "manager_spot_price" {
  type        = string
  default     = ""
  description = "(Optional; Default: On-demand price) The maximum price to use for reserving spot instances. If omitted then spot instance will not be used."
}

variable "manager_root_volume_type" {
  type        = string
  default     = "gp3"
  description = "(Optional; Default: gp3) The type of the root volume."
}

variable "manager_root_volume_size" {
  type        = number
  default     = 8
  description = "(Optional; Default: 8) The size of the root volume in gigabytes."
}

variable "runner_role" {
  type        = string
  default     = "runner"
  description = "(Optional; Default: runner) Name of the role the runner will be performing."
}

variable "runner_instance_type" {
  type        = string
  description = "The type of EC2 instance to use for the runner. If the instance type changes, a new launch configuration will be created and added to the ASG."
}

variable "runner_spot_price" {
  type        = string
  default     = ""
  description = "(Optional; Default: On-demand price) The maximum price to use for reserving spot instances. If omitted then spot instance will not be used."
}

variable "runner_root_volume_type" {
  type        = string
  default     = "gp3"
  description = "(Optional; Default: gp3) The type of the root volume."
}

variable "runner_root_volume_size" {
  type        = number
  default     = 16
  description = "(Optional; Default: 16) The size of the root volume in gigabytes."
}

variable "runner_ami_id" {
  type        = string
  default     = ""
  description = "(Optional; Default: ubuntu-latest) Specifies the AMI ID that is used by the runner to provision docker machine. An empty value means to use latest ubuntu image."
}

variable "runner_ssh_user" {
  type        = string
  default     = "ubuntu"
  description = "(Optional; Default: ubuntu) Specifies the username that is used by the runner to provision docker machine."
}

variable "ssh_public_key" {
  type        = string
  description = "Contains the public key portion of the key pair that is used by the runner to provision docker machine."
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "(Optional) Additional tags to attach to all resources created by this module."
}
