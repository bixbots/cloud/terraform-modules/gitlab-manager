data "aws_iam_policy_document" "secrets" {
  statement {
    actions = [
      "secretsmanager:GetSecretValue"
    ]
    resources = [data.aws_secretsmanager_secret.primary.arn]
  }
}

resource "aws_iam_policy" "secrets" {
  name   = "${module.launch_pad.name}-secrets"
  policy = data.aws_iam_policy_document.secrets.json
}

resource "aws_iam_role_policy_attachment" "secrets" {
  policy_arn = aws_iam_policy.secrets.arn
  role       = module.launch_pad.iam_role_id
}

# https://github.com/docker/machine/issues/1655
data "aws_iam_policy_document" "docker_machine" {
  statement {
    actions = [
      "ec2:DescribeSubnets",

      "ec2:DescribeInstances",
      "ec2:RunInstances",
      "ec2:StartInstances",
      "ec2:StopInstances",
      "ec2:RebootInstances",
      "ec2:TerminateInstances",

      "ec2:CreateTags",

      "ec2:DescribeSecurityGroups",
      "ec2:CreateSecurityGroup",
      "ec2:AuthorizeSecurityGroupIngress",

      "ec2:DescribeKeyPairs",
      "ec2:CreateKeyPair",
      "ec2:ImportKeyPair",
      "ec2:DeleteKeyPair",

      "ec2:RequestSpotInstances",
      "ec2:DescribeSpotInstanceRequests",
      "ec2:CancelSpotInstanceRequests",

      "iam:PassRole"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "docker_machine" {
  name   = "${module.launch_pad.name}-docker-machine"
  policy = data.aws_iam_policy_document.docker_machine.json
}

resource "aws_iam_role_policy_attachment" "docker_machine" {
  policy_arn = aws_iam_policy.docker_machine.arn
  role       = module.launch_pad.iam_role_id
}

# runner

data "aws_iam_policy_document" "runner_assume_role_ec2" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "runner" {
  name               = local.runner_name
  assume_role_policy = data.aws_iam_policy_document.runner_assume_role_ec2.json
  tags               = local.runner_tags
}

resource "aws_iam_role_policy_attachment" "runner_admin" {
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
  role       = aws_iam_role.runner.id
}

resource "aws_iam_role_policy_attachment" "runner_ssm_core" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  role       = aws_iam_role.runner.id
}

resource "aws_iam_instance_profile" "runner" {
  name = aws_iam_role.runner.name
  role = aws_iam_role.runner.name
}
