resource "aws_security_group_rule" "manager_allow_ssh_from_all" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group" "runner" {
  vpc_id = module.launch_pad.vpc_id
  name   = local.runner_name

  tags = merge(module.launch_pad.tags, {
    "Name" = local.runner_name
  })
}

resource "aws_security_group_rule" "runner_allow_all_egress" {
  security_group_id = aws_security_group.runner.id

  type      = "egress"
  from_port = 0
  to_port   = 0
  protocol  = "ALL"

  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "runner_allow_ssh_from_all" {
  security_group_id = aws_security_group.runner.id

  type      = "ingress"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "runner_allow_docker_from_all" {
  security_group_id = aws_security_group.runner.id

  type      = "ingress"
  from_port = 2376
  to_port   = 2376
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}
