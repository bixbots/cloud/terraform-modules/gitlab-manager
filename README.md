# gitlab-manager

## Summary

This module creates a GitLab CI/CD manager that uses docker-machine to provision EC2 instances on-demand that perform the actual builds using docker.

## Resources

In addition to these dependent modules:

* [ssh-keypair](https://gitlab.com/bixbots/cloud/terraform-modules/ssh-keypair)
* [tags-base](https://gitlab.com/bixbots/cloud/terraform-modules/tags-base)
* [launch-pad](https://gitlab.com/bixbots/cloud/terraform-modules/launch-pad)
* [cloud-init-dns](https://gitlab.com/bixbots/cloud/terraform-modules/cloud-init-dns)
* [s3-bucket](https://gitlab.com/bixbots/cloud/terraform-modules/s3-bucket)
* [standalone-ec2](https://gitlab.com/bixbots/cloud/terraform-modules/standalone-ec2)

This module creates the following resources:

* [aws_security_group_rule](https://www.terraform.io/docs/providers/aws/r/security_group_rule.html)
* [aws_security_group](https://www.terraform.io/docs/providers/aws/r/security_group.html)
* [aws_iam_policy](https://www.terraform.io/docs/providers/aws/r/iam_policy.html)
* [aws_iam_role_policy_attachment](https://www.terraform.io/docs/providers/aws/r/iam_role_policy_attachment.html)
* [aws_iam_role](https://www.terraform.io/docs/providers/aws/r/iam_role.html)
* [aws_iam_instance_profile](https://www.terraform.io/docs/providers/aws/r/iam_instance_profile.html)

## Cost

Use of this module will result in billable resources being launched in AWS. Before using this module, please familiarize yourself with the expenses associated with EC2 instances and autoscaling groups.

* AWS EC2 pricing information is available [here](https://aws.amazon.com/ec2/pricing/)

The manager EC2 instance will be long-running and available 24/7. The runner EC2 instances will be provisioned by docker-machine as needed on-demand and therefore short-lived.

## Details

Prior to using this module, a secret must be manually added to AWS secrets manager that contains the following:

* Secret name: `gitlab-{environment}-{manager_role}` (i.e. `gitlab-management-manager`)
* Secret key-value pairs:
  * `registration-token` Contains the registration token value from CI/CD settings page on gitlab.com for your repo/group.
  * `ssh-private-key` Contains the SSH private key from the generated key pair using PEM format. Make sure to encode newlines as `\n`.

## Inputs

| Name                     | Description                                                                                                                                     | Type         | Default     | Required |
|:-------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------|:-------------|:------------|:---------|
| environment              | Name of the environment the resource(s) will be a part of.                                                                                      | `string`     | -           | yes      |
| lifespan                 | The intended lifespan of the resource(s).                                                                                                       | `string`     | `permanent` | no       |
| subnet_type              | Specifies which subnet, either `public`, `private`, or `data`, the instance(s) are launched in.                                                 | `string`     | `private`   | no       |
| manager_role             | Name of the role the manager will be performing.                                                                                                | `string`     | `manager`   | no       |
| manager_instance_type    | The type of EC2 instance to use for the manager. If the instance type changes, a new launch configuration will be created and added to the ASG. | `string`     | -           | yes      |
| manager_spot_price       | The maximum price to use for reserving spot instances. If omitted then spot instance will not be used.                                          | `string`     | -           | no       |
| manager_root_volume_type | The type of the root volume.                                                                                                                    | `string`     | `gp3`       | no       |
| manager_root_volume_size | The size of the root volume in gigabytes.                                                                                                       | `number`     | 8           | no       |
| runner_role              | Name of the role the runner will be performing.                                                                                                 | `string`     | `runner`    | no       |
| runner_instance_type     | The type of EC2 instance to use for the runner. If the instance type changes, a new launch configuration will be created and added to the ASG.  | `string`     | -           | yes      |
| runner_spot_price        | The maximum price to use for reserving spot instances. If omitted then spot instance will not be used.                                          | `string`     | -           | no       |
| runner_root_volume_type  | The type of the root volume.                                                                                                                    | `string`     | `gp3`       | no       |
| runner_root_volume_size  | The size of the root volume in gigabytes.                                                                                                       | `number`     | 16          | no       |
| runner_ami_id            | Specifies the AMI ID that is used by the runner to provision docker machine. An empty value means to use latest ubuntu image.                   | `string`     | -           | no       |
| runner_ssh_user          | Specifies the username that is used by the runner to provision docker machine.                                                                  | `string`     | `ubuntu`    | no       |
| ssh_public_key           | Contains the public key portion of the key pair that is used by the runner to provision docker machine.                                         | `string`     | -           | yes      |
| tags                     | Additional tags to attach to all resources created by this module.                                                                              | `map(string` | -           | no       |

## Outputs

This module doesn't output anything.

## Examples

### Simple Usage

```terraform
module "gitlab_manager" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/gitlab-manager.git?ref=v1"

  environment   = "example"

  manager_instance_type = "t3.nano"
  manager_spot_price    = "0.0052"

  runner_instance_type = "t3.medium"
  runner_spot_price    = "0.0416"

  ssh_public_key  = file("${path.module}/example_key.pub")

  tags = {
    "TerraformModule" = "example/gitlab-manager"
  }
}
```

## Version History

* v1 - Initial Release
