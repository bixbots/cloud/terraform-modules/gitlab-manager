locals {
  application = "gitlab"
}

module "ssh_keypair" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/ssh-keypair.git?ref=v1"

  application = local.application
  environment = var.environment
  role        = var.manager_role
  lifespan    = var.lifespan

  public_key = var.ssh_public_key
}

module "tags_base" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/tags-base.git?ref=v1"

  application = local.application
  environment = var.environment
  lifespan    = var.lifespan
  tags        = var.tags
}

module "launch_pad" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/launch-pad.git?ref=v1"

  application = local.application
  environment = var.environment
  role        = var.manager_role

  iam_role_description = ""
  assume_role_policy   = ""
  enable_admin_access  = true
  allow_all_egress     = true

  tags = module.tags_base.tags
}

module "cloud_init_dns" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/cloud-init-dns?ref=v1"

  application = local.application
  environment = var.environment
  role        = var.manager_role

  subnet_type = var.subnet_type
  iam_role_id = module.launch_pad.iam_role_id
}

locals {
  runner_name = "${local.application}-${var.environment}-${var.runner_role}"
  runner_tags = merge(module.launch_pad.tags, {
    "Name" = local.runner_name
    "Role" = var.runner_role
  })
  runner_tags_flatten = flatten([
    for tag_key, tag_value in local.runner_tags : [
      "${tag_key},${tag_value}"
    ]
  ])
  runner_tags_final = join(",", local.runner_tags_flatten)
}

module "runner_cache" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/s3-bucket?ref=v1"

  bucket_name = "${local.runner_name}-cache"

  force_destroy               = true
  expiration_days             = 365
  expiration_days_noncurrent  = 365
  transition_days_standard_ia = 365
  transition_days_glacier     = 365

  admin_policy = {
    enabled     = true
    iam_role_id = module.launch_pad.iam_role_id
  }

  tags = module.launch_pad.tags
}

module "standalone_ec2" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/standalone-ec2?ref=v1"

  name        = module.launch_pad.name
  vpc_id      = module.launch_pad.vpc_id
  subnet_type = var.subnet_type

  spot_price    = var.manager_spot_price
  instance_type = var.manager_instance_type
  root_block_device = {
    enabled     = true
    volume_type = var.manager_root_volume_type
    volume_size = var.manager_root_volume_size
  }
  image_id = data.aws_ami.manager.id
  key_name = module.ssh_keypair.key_name

  security_group_ids      = module.launch_pad.security_group_ids
  iam_instance_profile_id = module.launch_pad.iam_instance_profile_id

  rendered_cloud_init = concat([module.cloud_init_dns.part], [{
    filename     = "configure-manager.sh"
    content_type = "text/x-shellscript"
    content = templatefile("${path.module}/configure-manager.sh", {
      environment          = var.environment
      manager_name         = trimsuffix(module.cloud_init_dns.fqdn, ".")
      aws_region           = data.aws_region.current.name
      aws_vpc_id           = module.launch_pad.vpc_id
      aws_subnet_type      = var.subnet_type
      aws_instance_type    = var.runner_instance_type
      aws_spot_enable      = length(var.runner_spot_price) > 0
      aws_spot_price       = var.runner_spot_price
      aws_root_volume_type = var.runner_root_volume_type
      aws_root_volume_size = var.runner_root_volume_size
      aws_ami_id           = var.runner_ami_id
      aws_ssh_user         = var.runner_ssh_user
      aws_tags             = local.runner_tags_final
      aws_instance_profile = aws_iam_instance_profile.runner.name
      aws_security_group   = aws_security_group.runner.name
      aws_secret_name      = data.aws_secretsmanager_secret.primary.id
      aws_keypair_name     = module.ssh_keypair.key_name
      aws_public_key       = var.ssh_public_key
      aws_cache_bucket     = module.runner_cache.bucket_id
    })
  }])

  tags = module.launch_pad.tags
}
