#!/usr/bin/env bash

ENVIRONMENT="${environment}"
MANAGER_NAME="${manager_name}"

export AWS_DEFAULT_REGION="${aws_region}"
AWS_VPC_ID="${aws_vpc_id}"
AWS_SUBNET_TYPE="${aws_subnet_type}"
AWS_INSTANCE_TYPE="${aws_instance_type}"
AWS_SPOT_ENABLE="${aws_spot_enable}"
AWS_SPOT_PRICE="${aws_spot_price}"
AWS_ROOT_TYPE="${aws_root_volume_type}"
AWS_ROOT_SIZE="${aws_root_volume_size}"
AWS_AMI="${aws_ami_id}"
AWS_SSH_USER="${aws_ssh_user}"
AWS_TAGS="${aws_tags}"
AWS_INSTANCE_PROFILE="${aws_instance_profile}"
AWS_SECURITY_GROUP="${aws_security_group}"
AWS_SECRET_NAME="${aws_secret_name}"
AWS_KEYPAIR_NAME="${aws_keypair_name}"
AWS_PUBLIC_KEY="${aws_public_key}"
AWS_CACHE_BUCKET="${aws_cache_bucket}"

curl -sSL https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
sudo yum update -y
sudo yum install -y jq gitlab-runner

AWS_INSTANCE_ID=$(curl -sS http://169.254.169.254/latest/meta-data/instance-id)
AWS_AZ_NAME=$(curl -sS http://169.254.169.254/latest/meta-data/placement/availability-zone)
AWS_SUBNET_JSON=$(aws ec2 describe-subnets --filters Name=vpc-id,Values=$AWS_VPC_ID Name=availability-zone,Values=$AWS_AZ_NAME Name=tag:Type,Values=$AWS_SUBNET_TYPE --output json)
AWS_SUBNET_ID=$(echo "$AWS_SUBNET_JSON" | jq -r .Subnets[0].SubnetId)
AWS_ZONE_SUFFIX=$(echo "$AWS_SUBNET_JSON" | jq -r '.Subnets[0].Tags[] | select(.Key == "AvailabilityZoneSuffix") | .Value')

sudo amazon-linux-extras install -y docker
sudo service docker start
sudo usermod -a -G docker ec2-user
sudo docker info
sudo chkconfig docker on

URL_BASE=https://github.com/docker/machine/releases/download/v0.16.2
curl -sSL $URL_BASE/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine
sudo mv /tmp/docker-machine /usr/local/bin/docker-machine
chmod +x /usr/local/bin/docker-machine

mkdir -p /etc/gitlab-runner

AWS_KEYPATH=/etc/gitlab-runner/id_rsa
AWS_PUBLIC_KEY=$(echo "$AWS_PUBLIC_KEY" | tr -d '\r')
AWS_PRIVATE_KEY=$(aws secretsmanager get-secret-value --secret-id "$AWS_SECRET_NAME" --version-stage AWSCURRENT | jq -r '.SecretString | fromjson | ."ssh-private-key"' | tr -d '\r')
REGISTRATION_TOKEN=$(aws secretsmanager get-secret-value --secret-id "$AWS_SECRET_NAME" --version-stage AWSCURRENT | jq -r '.SecretString | fromjson | ."registration-token"')

echo "$AWS_PRIVATE_KEY" > $AWS_KEYPATH
echo "$AWS_PUBLIC_KEY" > $AWS_KEYPATH.pub

cat > /etc/gitlab-runner/config.toml <<EOF
concurrent = 10
check_interval = 5

[session_server]
  session_timeout = 1800
EOF

cat > /etc/gitlab-runner/config-template.toml <<EOF
concurrent = 10
check_interval = 5

[[runners]]
	name = "$AWS_INSTANCE_ID"
	url = "https://gitlab.com/"
	executor = "docker+machine"
	limit = 0
	environment = [
		"ENVIRONMENT=$ENVIRONMENT",
		"AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION",
		"AWS_VPC_ID=$AWS_VPC_ID",
		"AWS_SUBNET_ID=$AWS_SUBNET_ID"
	]

	[runners.docker]
		image = "alpine:latest"
		privileged = true
		disable_cache = false

	[runners.cache]
		Type = "s3"
		Shared = true

	[runners.cache.s3]
		ServerAddress = "s3.amazonaws.com"
		BucketName = "$AWS_CACHE_BUCKET"
		BucketLocation = "$AWS_DEFAULT_REGION"
		Insecure = false

	[runners.machine]
		IdleCount = 0
		IdleTime = 300
		MaxBuilds = 10
		MachineDriver = "amazonec2"
		MachineName = "gitlab-runner-%s"
		MachineOptions = [
			"amazonec2-region=$AWS_DEFAULT_REGION",
			"amazonec2-vpc-id=$AWS_VPC_ID",
			"amazonec2-subnet-id=$AWS_SUBNET_ID",
			"amazonec2-zone=$AWS_ZONE_SUFFIX",
			"amazonec2-private-address-only=true",
			"amazonec2-use-private-address=true",
			"amazonec2-tags=$AWS_TAGS",
			"amazonec2-iam-instance-profile=$AWS_INSTANCE_PROFILE",
			"amazonec2-security-group=$AWS_SECURITY_GROUP",
			"amazonec2-instance-type=$AWS_INSTANCE_TYPE",
			"amazonec2-volume-type=$AWS_ROOT_TYPE",
			"amazonec2-root-size=$AWS_ROOT_SIZE",
			"amazonec2-ami=$AWS_AMI",
			"amazonec2-ssh-user=$AWS_SSH_USER",
			"amazonec2-keypair-name=$AWS_KEYPAIR_NAME",
			"amazonec2-ssh-keypath=$AWS_KEYPATH",
			"amazonec2-request-spot-instance=$AWS_SPOT_ENABLE",
			"amazonec2-spot-price=$AWS_SPOT_PRICE"
		]
EOF

chown -R gitlab-runner:gitlab-runner /etc/gitlab-runner
chmod -R 600 /etc/gitlab-runner

cat > /etc/init.d/gitlab-manager <<EOF
#!/usr/bin/env bash
#
#	/etc/rc.d/init.d/gitlab-manager
#
#	TODO

# chkconfig: 345 26 74
# description: Start script for launch my service

### BEGIN INIT INFO
# Provides:          gitlab-runner
# Required-Start:    \$remote_fs \$syslog
# Required-Stop:     \$remote_fs \$syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: TODO
# Description:       TODO
### END INIT INFO

start() {
    echo -n "Registering gitlab runner..."
    touch /var/lock/subsys/gitlab-manager

	gitlab-runner register \
		--non-interactive \
		--template-config /etc/gitlab-runner/config-template.toml \
		--url https://gitlab.com \
		--registration-token "$${REGISTRATION_TOKEN}" \
		--executor docker+machine \
		--docker-image alpine:latest \
		--name "$${AWS_INSTANCE_ID}" \
		--tag-list "docker,aws,$${ENVIRONMENT},$${AWS_DEFAULT_REGION}" \
		--run-untagged="false" \
		--locked="false" \
		--access-level="not_protected"

    return \$?
}

stop() {
    echo -n "Unregistering all gitlab runners..."
    rm -rf /var/lock/subsys/gitlab-manager
    gitlab-runner unregister --all-runners
    return \$?
}

case "\$1" in
    start)
    start
    ;;
    stop)
    stop
    ;;
    *)
    echo "Usage: \$0 {start|stop}"
    exit 1
    ;;
esac
EOF

chmod +x /etc/init.d/gitlab-manager
chkconfig --add gitlab-manager
chkconfig gitlab-manager --level 12345 on
chkconfig gitlab-manager --level 06 off
service gitlab-manager start
