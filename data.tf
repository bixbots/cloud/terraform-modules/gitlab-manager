data "aws_region" "current" {}

data "aws_vpc" "primary" {
  tags = {
    Name = var.environment
  }
}

data "aws_secretsmanager_secret" "primary" {
  name = module.launch_pad.name
}

data "aws_ami" "manager" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}
